# ZCU102 LED Demo

This is a Peary device that is intended to allow basic testing
of the PS-PL interface by flashing some LEDs on the ZCU102.
The associated firmware is in /demo2_led/