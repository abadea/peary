/**
 * Caribou SPI interface class implementation
 */

#define SPI_BUS_EMULATED

#include "spi_bus.hpp"

using namespace caribou;

#include "spi_bus.cpp"
